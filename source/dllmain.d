import core.sys.windows.windows, core.sys.windows.dll;
import std.concurrency : spawn, yield;
import core.runtime;

import llmo;


__gshared ubyte[] bordeds_bak;

extern (Windows) BOOL DllMain(HINSTANCE hInstance, ULONG ulReason, LPVOID){
	final switch (ulReason)
	{
		case DLL_PROCESS_ATTACH:
			Runtime.initialize();
			spawn({
				while (*cast(uint*)0xC8D4C0 < 9)
					yield();
				init();
			});
			dll_process_attach(hInstance, true);
			break;
		case DLL_PROCESS_DETACH:
			uninit();
			Runtime.terminate();
			dll_process_detach(hInstance, true);
			break;
		case DLL_THREAD_ATTACH:
			dll_thread_attach(true, true);
			break;
		case DLL_THREAD_DETACH:
			dll_thread_detach(true, true);
			break;
	}
	return true;
}

/// Uninitialize plugin: restore original code
void uninit(){
	// Enable borders
	if (bordeds_bak.length == 685)
		writeMemory(0x575B6A, bordeds_bak, MemorySafe.safe);
	writeMemory(0x575E1C, [0x83, 0xC4, 0x08], MemorySafe.safe);

	// Restore border spaces
	writeMemory(cast(ubyte*)0x57526C, cast(ubyte)0x75);
	writeMemory(cast(float**)0x5752EE, cast(float*)0x8652BC);
	writeMemory(cast(float**)0x575313, cast(float*)0x865370);
	writeMemory(cast(ubyte*)0x57532C, cast(ubyte)0x75);
	writeMemory(cast(float**)0x57533E, cast(float*)0x8652B8);
	writeMemory(cast(float**)0x575363, cast(float*)0x86536C);
}

/// Initialize plugin: set hacks
void init(){
	// Values for fill map
	__gshared float start = 0.0f, end = 1.0f;

	// Disable borders
	bordeds_bak = readMemory(0x575B6A, 685, MemorySafe.safe);
	setMemory(0x575B6A, 0x90, 685, MemorySafe.safe);
	setMemory(0x575E1C, 0x90, 3, MemorySafe.safe);

	// Fill border spaces
	writeMemory(cast(ubyte*)0x57526C, cast(ubyte)0xEB);
	writeMemory(cast(float**)0x5752EE, &start);
	writeMemory(cast(float**)0x575313, &end);
	writeMemory(cast(ubyte*)0x57532C, cast(ubyte)0xEB);
	writeMemory(cast(float**)0x57533E, &start);
	writeMemory(cast(float**)0x575363, &end);
}